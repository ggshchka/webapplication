name := "WebApplication"

version := "0.1"

scalaVersion := "2.13.5"


import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val tylipPublic =
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"


val shared = crossProject(JSPlatform, JVMPlatform)
  .crossType(CrossType.Full).in(file("."))
  .settings(
    libraryDependencies ++= Seq(
      "org.endpoints4s" %%% "algebra" % "1.3.0",
      "io.circe" %%% "circe-generic" % "0.13.0",
      "io.circe" %%% "circe-generic" % "0.13.0",
      "io.circe" %%% "circe-literal" % "0.13.0",
      "io.circe" %%% "circe-generic-extras" % "0.13.0",
      "org.endpoints4s" %%% "algebra-circe" % "1.3.0",
      "org.endpoints4s" %%% "json-schema-generic" % "1.3.0",

      "org.scala-lang.modules" %%% "scala-parser-combinators" % "1.1.2",
      "org.scalacheck" %%% "scalacheck" % "1.14.3",
    ),
    scalacOptions ++=
      "-encoding" :: "UTF-8" ::
        "-Ypartial-unification" ::
        Nil,
  ).jvmSettings(Seq())


val client = shared.js
  .enablePlugins(ScalaJSBundlerPlugin)
  .disablePlugins(RevolverPlugin)
  .settings(
    resolvers += "jitpack" at "https://jitpack.io",
    libraryDependencies ++= Seq(
      "com.github.outwatch.outwatch" %%% "outwatch" % "61deece8",
      "com.github.outwatch.outwatch" %%% "outwatch-monix" % "master-SNAPSHOT",

      "org.endpoints4s" %%% "xhr-client" % "2.0.0",
      "org.endpoints4s" %%% "xhr-client-faithful" % "2.0.0",
    ),
    npmDependencies in Compile ++= Seq(
      "jquery" -> "3.3",
      "bootstrap" -> "4.3"
    ),
    useYarn := true,
    requireJsDomEnv in Test := true,
    scalaJSUseMainModuleInitializer := true,
    scalaJSLinkerConfig ~= (_.withModuleKind(ModuleKind.CommonJSModule)),// configure Scala.webapp.js to emit a JavaScript module instead of a top-level script
    version in webpack := "4.43.0",
    webpackConfigFile in fastOptJS := Some(baseDirectory.value / "webpack.config.dev.js"),
    webpackBundlingMode in fastOptJS := BundlingMode.LibraryOnly()
)

val server = shared.jvm
  .enablePlugins(JavaAppPackaging)
  .settings(
    resolvers += tylipPublic,
    mainClass in reStart := Some("sample.WebAppJVM"),
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-dsl" % "0.21.9",
      "org.http4s" %% "http4s-blaze-server" % "0.21.9",
      "org.http4s" %% "http4s-circe" % "0.21.9",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "org.slf4j" % "slf4j-nop" % "1.7.30",

      "org.endpoints4s" %% "http4s-server" % "5.0.0",
    ),
  )

//target in Compile := (target in(server, Compile)).value

addCommandAlias("dev", "; compile; fastOptJS::startWebpackDevServer; devwatch; fastOptJS::stopWebpackDevServer")
addCommandAlias("devwatch", "~; fastOptJS; copyFastOptJS")

lazy val copyFastOptJS = TaskKey[Unit]("copyFastOptJS", "Copy javascript files to target directory")
copyFastOptJS := {
  val inDir = (target in(client, Compile)).value / "scala-2.12" / "scalajs-bundler" / "main"
  val outDir = (target in(client, Compile)).value / "dev"
  val files = Seq("shared" + "-fastopt-loader.js", "shared" + "-fastopt.js") map { p => (inDir / p, outDir / p) }
  IO.copy(files, overwrite = true, preserveLastModified = true, preserveExecutable = true)
}