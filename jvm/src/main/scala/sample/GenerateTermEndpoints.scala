package sample

import cats.effect.{Blocker, ContextShift, IO}
import endpoints4s.http4s.server.{Endpoints, JsonEntitiesFromCodecs, JsonEntitiesFromSchemas}
import org.http4s.HttpRoutes
import sample.core.ContextTerm.generateContextTerm
import sample.core.{Generator, Parser, Semantics, Term}
import sample.model._

import scala.util.Random
import scala.concurrent.ExecutionContext

object GenerateTermEndpoints extends Endpoints[IO]
  with JsonEntitiesFromCodecs
  with EndpointsApi  {

  implicit val ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  def getRedStratFunc(str: String): Term[Nothing] => Term[Nothing] =
    str match {
      case "normal order" => Semantics.normReduction
      case "application order" => Semantics.applReduction
      case "call-by-name" => Semantics.cbnReduction
      case "call-by-value" => Semantics.cbvReduction
    }

  def endpoints(): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      routesFromEndpoints(
        reductionEndpoint.implementedBy {
          t: TermForRqRs =>
            try {
              Some(Semantics.getEvaluateList[Nothing](
                Parser(t.term).get,
                getRedStratFunc(t.redStrat),
              ).map(x => TermForRqRs(x.toString, t.redStrat)))
            } catch {
              case e: Exception =>
                None
            }
        },

        generateTermEndpoint.implementedBy{
          c =>
            try {
              val redStrategies = List("normal order", "application order", "call-by-name", "call-by-value")
              val redStratStr = redStrategies((new Random).nextInt(redStrategies.length))
              val term = Generator.genByCountOfSteps(
                c.cos+3, c.cos+3,exactly = false, c.cos,
                getRedStratFunc(redStratStr)
              )

              Some(TermForRqRs(term.toString, redStratStr))
            } catch {
              case e: Exception  => None
            }
        },

        evaluatedTermEndpoint.implementedBy{
          t =>
            try {
              val evalByUser = Parser(t.evaluatedByUser.term).get
              val evalByMachine = Semantics.evaluate[Nothing](
                Parser(t.generatedTerm.term).get,
                getRedStratFunc(t.generatedTerm.redStrat),
              )
              val evalByMachineWSteps = Semantics.getEvaluateList[Nothing](
                Parser(t.generatedTerm.term).get,
                getRedStratFunc(t.generatedTerm.redStrat),
              ).map(x => TermForRqRs(x.toString, t.generatedTerm.redStrat))
              Some(ResultOfEvaluating(
                if (evalByUser == evalByMachine) "Правильно" else "Неверно",
                evalByMachineWSteps
              ))
            } catch {
              case e: Exception => None
            }
        },

        generateTasksEndpoint.implementedBy{
          _ =>
            def getReductionTask(cos:Int): ReductionTask = {
              val redStrategies = List("normal order", "application order", "call-by-name", "call-by-value")
              val redStratStr = redStrategies((new Random).nextInt(redStrategies.length))
              val term = Generator.genByCountOfSteps(
                cos+3, cos+3,exactly = false, cos,
                getRedStratFunc(redStratStr)
              )
              ReductionTask(term.toString, redStratStr, "")
            }
            def getContextTask(cos:Int): ContextTask = {
              val redStrategies = List("normal order", "application order", "call-by-name", "call-by-value")
              val redStratStr = redStrategies((new Random).nextInt(redStrategies.length))
              val cntxTerm = generateContextTerm(cos, getRedStratFunc(redStratStr))
              ContextTask(cntxTerm._1, cntxTerm._2, redStratStr, List())
            }
            List(
              getReductionTask(3),
              getReductionTask(4),
              getReductionTask(5),
              getContextTask(3),
              getContextTask(4),
              getContextTask(5)
            )
        },

        performTaskEndpoint.implementedBy{
          case ReductionTask(term,redStrat,clientAnswer) =>
            Parser(clientAnswer) match {
              case Some(t) => {
                val evalByMachineTerm = Semantics.evaluate[Nothing](Parser(term).get, getRedStratFunc(redStrat))
                if (t == evalByMachineTerm) TaskResult("Правильно")
                else TaskResult("Неправильно")
              }
              case None => TaskResult("Неправильно введен терм")
            }
          case ContextTask(contextTermList,evaluatedTerm,redStrat,clientAnswer) => {
            var i = -1
            val resTerm = contextTermList.map(x =>
              if (x == "hole") {
                i += 1
                clientAnswer(i)
              } else x
            ).mkString
            Parser(resTerm) match {
              case Some(t) => {
                val evalT =  Semantics.evaluate[Nothing](t, getRedStratFunc(redStrat))
                if (evalT == Parser(evaluatedTerm).get) TaskResult("Правильно")
                else TaskResult("Неправильно")
              }
              case None => TaskResult("Неправильный ввод")
            }
          }
        }
      )
    }

}


