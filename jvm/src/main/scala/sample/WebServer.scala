package sample

import cats.effect.{Blocker, ExitCode, IO, IOApp, Resource}
import cats.syntax.semigroupk._
import org.http4s.{HttpRoutes, StaticFile}
import org.http4s.dsl.io._
import org.http4s.server.blaze._
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.middleware.CORS
import org.http4s.server.staticcontent.{FileService, fileService}

import endpoints4s.{algebra, generic}


import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.global



object WebServer extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    server.use(_ => IO.never).as(ExitCode.Success)


  private def server: Resource[IO, Server[IO]] =
    for {
      blocker <- Blocker[IO]
      httpApp = (
        GenerateTermEndpoints.endpoints
        ).orNotFound
      server <- BlazeServerBuilder[IO](ExecutionContext.global)
        .bindHttp(8080)
        .withHttpApp(CORS(httpApp))
        .resource
    } yield server
}
