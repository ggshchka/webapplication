package sample.SetOfTasks
import cats.effect.{ContextShift, IO, SyncIO}
import colibri.ext.monix
import colibri.Observable
import outwatch.dsl.{onClick, _}
import outwatch._
import outwatch.reactive.handler._
import sample.XHRClient.XHRClient._
import sample.js.ReductionRouter.{NextPage, Page}
import sample.model.{ContextTask, CountOfSteps, ReductionTask, ResultOfEvaluating, TaskResult, TasksForRqRs, TermForRqRs, TermsForChecking}



final case class SetOfTasksComponent private(
                                         sink: Handler[Observable[Option[ResultOfEvaluating]]],
                                         nextPageHandler: Handler[Page],
                                         gradeHandler: Handler[Int]
                                       ) {
  val gradeStream = Handler.unsafe[IO[Int]](IO(0))

  def reductionTaskNode(
                       task: ReductionTask
                       ) = {
    for {
      inputHandler <- Handler.create[String]("")
      inputHandlerHelper <- Handler.create[String]("")
      disableHandler <- Handler.create[Boolean](false)
      resultHandler <- Handler.create[IO[TaskResult]]
    } yield
    div(
      ul(
        cls := "context-wrapper content-res-wrapper",
        li(
          cls := "row d-flex justify-content-center",
          "Вычислите терм, применяя стратегию вычисления - " + task.redStrat + ":"
        ),
        li(
          cls := "row d-flex justify-content-center term",
          task.term,
        ),
        hr(cls := "row d-flex justify-content-center solid"),
        li(
          cls := "row d-flex justify-content-center",
          textArea(
            `type` := "text",
            cls := "form-control mb-3 col-5 term",
            placeholder := "Ответ...",
            onInput.value --> inputHandler,
            value <-- inputHandler
          ),
        ),
        li(
          cls := "row d-flex justify-content-center",
          button(
            "Отправить",
            `type` := "submit",
            color := "#000000",
            backgroundColor := "#CECECE",
            cls := "btn btn-secondary",
            disabled <-- disableHandler,
            onClick.use(true) --> disableHandler,
            onClick(inputHandler) --> inputHandlerHelper,
            onClick(inputHandlerHelper).map(y =>
              getResultOfPerforming(ReductionTask(task.term, task.redStrat, y))
            ) --> resultHandler,
//            resultHandler.map(_.map(y =>
//              if (y.message == "Правильно") onClick(gradeStream.map(x => x.map(_+1))) --> gradeStream
//              else onClick(gradeStream.map(x => x.map(_-1))) --> gradeStream
//            )),
            onClick(inputHandler.map(x => "")) --> inputHandler,
          ),
        ),
        resultHandler.map(_.map(y => div(y.message)))
      ),
    )
  }

  def cntxInputField(cntx: Handler[String]): HtmlVNode = input(
    cls := "term",
    `type` := "text",
    color := "",
    borderBottom := "",
    attr("animation") := "underliner_unsolved 2s steps(1) infinite",
    attr("transform") := "translateZ(0)",
    attr("will-change") := "transform, border-bottom",
    backgroundColor := "#a9b0c1",
    color := "#fff",
    textAlign := "center",
    lineHeight := "11px",
    verticalAlign := "middle",
    padding := "2px 0px",
    marginLeft := "",
    marginRight := "",
    border := "0px solid #999",
    borderRadius := "4px",
    width := "25px",
    onInput.value --> cntx,
    value <-- cntx
  )

  def getCntxWithInputFields(cntxs: List[Handler[String]], contextTermList: List[String]) = {
    var counter = -1
    contextTermList.map{
      el => if (el != "hole") div(
        cls := "term",
        el
      ) else {
        counter += 1
        cntxInputField(cntxs(counter))
      }
    }
  }

  def contextTaskNode(
                     task: ContextTask
                     ) = {
    for {
      disableHandler <- Handler.create[Boolean](false)
      inputListHandler <- Handler.create[List[String]](List(""))
      resultHandler <- Handler.create[IO[TaskResult]]
      holesSize = task.contextTermList.count(x => x == "hole")
      l: List[Handler[String]] = List.range(0, holesSize).map(_ => Handler.unsafe[String])
    } yield
    div(
      ul(
        cls := "context-wrapper content-res-wrapper",
        li(
          cls := "row d-flex justify-content-center",
          "Заполните пропуски в терме так, чтобы вычисление этого терма стратегией - " + task.redStrat + ", равнялось второму терму:"
        ),
        li(
          cls := "row d-flex justify-content-center",
          getCntxWithInputFields(l, task.contextTermList)
        ),
        hr(cls := "row d-flex justify-content-center solid"),
        li(
          cls := "row d-flex justify-content-center",
          div(
            cls := "term",
            task.evaluatedTerm
          ),
        ),
        li(
          cls := "d-flex flex-row-reverse p-1",
          button(
            "Отправить",
            idAttr := "reductionButton",
            `type` := "submit",
            color := "#000000",
            backgroundColor := "#CECECE",
            cls := "btn btn-lg btn-secondary",
            disabled <-- disableHandler,
            onClick.use(true) --> disableHandler,
            onClick(inputListHandler.map(x => List())) --> inputListHandler,
            l.map(y => y.map(z => onClick(inputListHandler.map(x => x ++ List(z))) --> inputListHandler)),
            onClick(inputListHandler.map(y =>
              getResultOfPerforming(ContextTask(task.contextTermList, task.evaluatedTerm, task.redStrat, y))
            )) --> resultHandler,
            l.map(x => onClick(x).map(x => "") --> x)
            ),
        ),
        resultHandler.map(_.map(y => div(y.message)))
      )
    )
  }

  def node = Observable.fromSync(for {
    goHandler <- Handler.create[Boolean](false)
    counterHandler <- Handler.create[Int](0)
    finishHandler <- Handler.create[Boolean](false)
  } yield div(
    cls := "row d-flex flex-column col-9 ml-2",
    header(
      cls := "row d-flex justify-content-center subsystem-title",
      marginBottom := "1.14rem",
      h1("Выполнить задание"),
    ),
    finishHandler.map(if (_)
      div(
        gradeHandler.map(x => div(s"Конец"))
      )
    else div(
      goHandler.map( if (_) {
        div(
          getGeneratedTasks.map(
            lst => div(
              counterHandler.map (i =>
                lst(i) match {
                  case r@ReductionTask(_, _, _) => div(reductionTaskNode(r))
                  case c@ContextTask(_, _, _, _) => div(contextTaskNode(c))
                }
              ),
              ul(
                cls := "pagination float-right",
    //              li(
    //                cls := "page-item",
    //                a(
    //                  cls:="page-link",
    //                  href:="#",
    //                  "Назад",
    //                  onClick(counterHandler.map(x => if(x>0) x-1 else x)) --> counterHandler
    //                ),
    //              ),
                li(
                  cls := "page-item",
                  counterHandler.map(y =>
                    if (y == lst.length-1)
                      a(
                        cls:="page-link",
                        href:="#",
                        "Завершить",
                        onClick.use(true) --> finishHandler
                      )
                    else
                      a(
                        cls:="page-link",
                        href:="#",
                        "След.",
                        onClick(counterHandler.map(x => if(x<lst.length-1) x+1 else x)) --> counterHandler
                      )
                  ),
                )
              )
            )
          )
        )
      } else {
        button(
          "Начать",
          idAttr := "reductionButton",
          `type` := "submit",
          color := "#000000",
          backgroundColor := "#CECECE",
          cls := "btn btn-lg btn-secondary col",
          onClick.use(true) --> goHandler,
        )
      })

    )),
  ))

}


object SetOfTasksComponent{

  def init =
    for {
      sink <- Handler.create[Observable[Option[ResultOfEvaluating]]]
      page <- Handler.create[Page]
      grade <- Handler.create[Int](0)
    } yield SetOfTasksComponent(sink, page, grade)

}