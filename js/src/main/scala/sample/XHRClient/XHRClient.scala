package sample.XHRClient

import cats.effect.{ContextShift, IO}
import cats.effect.IO.fromFuture
import colibri.Observable
import sample.core.Term
import sample.model._

import scala.concurrent.ExecutionContext

object XHRClient {
  implicit val ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  def getEvaluatedTerm(t: TermForRqRs): IO[Option[List[TermForRqRs]]] =
    fromFuture(IO(XHRClientAPI.reductionEndpoint(t)))

  def getGeneratedTerm(cos: CountOfSteps): IO[Option[TermForRqRs]] =
    fromFuture(IO(XHRClientAPI.generateTermEndpoint(cos)))

  def getResultOfEvaluating(terms: TermsForChecking): IO[Option[ResultOfEvaluating]] =
    fromFuture(IO(XHRClientAPI.evaluatedTermEndpoint(terms)))

  // TASKS (GENERAL)
  def getGeneratedTasks: IO[List[TasksForRqRs]] =
    fromFuture(IO(XHRClientAPI.generateTasksEndpoint()))

  def getResultOfPerforming(t: TasksForRqRs): IO[TaskResult] =
    fromFuture(IO(XHRClientAPI.performTaskEndpoint(t)))
}
