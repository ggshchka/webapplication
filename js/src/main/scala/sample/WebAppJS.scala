package sample

import cats.effect.{ExitCode, IO, IOApp}
import outwatch._
import outwatch.dsl._
import sample.js.TopComponent

object WebAppJS extends IOApp {
  def run(args: List[String]): IO[ExitCode] = {
    for {
      topComponent <- TopComponent.init
      _ <- OutWatch.renderReplace[IO]("#app", div(topComponent.node))
    } yield ExitCode.Success
  }
}