package sample.api

import sample.core.{ContextTerm, Generator, Parser, Semantics, Term}

object CoreAPI {

  def evalTerm(t: String, redStrat: String): String =
    Parser.apply(t) match {
      case Some(i) =>
        redStrat match {
          case "normal order" =>
            Semantics.evaluate[Nothing](i, Semantics.normReduction:Term[Nothing] => Term[Nothing]).toString
          case "application order" =>
            Semantics.evaluate[Nothing](i, Semantics.applReduction:Term[Nothing] => Term[Nothing]).toString
          case "call-by-name" =>
            Semantics.evaluate[Nothing](i, Semantics.cbnReduction:Term[Nothing] => Term[Nothing]).toString
          case "call-by-value" =>
            Semantics.evaluate[Nothing](i, Semantics.cbvReduction:Term[Nothing] => Term[Nothing]).toString
        }
      case None => "Error: non-parseable"
    }

  def evalTermWithSteps(t: String, redStrat: String): List[String] =
    Parser.apply(t) match {
      case Some(i) =>
        redStrat match {
          case "normal order" =>
            Semantics.getEvaluateList[Nothing](i, Semantics.normReduction:Term[Nothing] => Term[Nothing]).map(_.toString)
          case "application order" =>
            Semantics.getEvaluateList[Nothing](i, Semantics.applReduction:Term[Nothing] => Term[Nothing]).map(_.toString)
          case "call-by-name" =>
            Semantics.getEvaluateList[Nothing](i, Semantics.cbnReduction:Term[Nothing] => Term[Nothing]).map(_.toString)
          case "call-by-value" =>
            Semantics.getEvaluateList[Nothing](i, Semantics.cbvReduction:Term[Nothing] => Term[Nothing]).map(_.toString)
        }
      case None => List("Error: non-parseable")
    }

  def getContextTerm[A](comp: A) = {
    val t = Generator.genByCountOfSteps(5,7,exactly = false,3, Semantics.normReduction)
    val res = Semantics.evaluate[Nothing](t,Semantics.normReduction:Term[Nothing] => Term[Nothing])
    val contextTerm = ContextTerm.getContext(t, "_None_")
    val termList = ContextTerm.getListContextTerm(t)
    val contextTermList = contextTerm._1.toString.split("_").toList
    (contextTermList.map(el => if (el == "None") comp else el),
      contextTerm._2, termList, contextTerm._3.map(_.toString), res.toString, contextTerm._1)
  }

  def checkResOfContext(cntxTerm: Any, strList: List[Any], res: Any): Boolean = {
    val strTerm = ContextTerm.suspToContextTerm(cntxTerm, strList, "_None_").toString
    val term = Semantics.evaluate[Nothing](Parser.apply(strTerm).get, Semantics.normReduction:Term[Nothing] => Term[Nothing])
    Parser.apply(res.toString) match {
      case Some(x) => x == term
      case None => false
    }
  }

  def generateTerm(countOfSteps: Int): String = {
    val term = Generator.genByCountOfSteps(7,7,exactly = false, countOfSteps, Semantics.normReduction)
    term.toString
  }

  def compareTerms(t1: String, t2: String): Boolean = {
    val term1 = Parser.apply(t1).get
    val term2 = Parser.apply(t2).get
    term1 == term2
  }
}
