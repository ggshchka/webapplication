package sample.js

import cats.effect.IO
import colibri.Observable
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import sample.SetOfTasks.SetOfTasksComponent
import sample.js.ReductionRouter._
import sample.js.TopComponent.pgeHndl
import sample.model.TermForRqRs

final case class TopComponent(
  headerComponent: HeaderComponent,
//  contextTermComponent: ContextTermComponent,
  setOfTasksComponent: SetOfTasksComponent,
  navComponent: VNode, //NavComponent,
  tasksComponent: VNode, //TasksComponent,
  reductionComponent: VNode,
  generatorComponent: VNode,
) {

  def node: Observable[VDomModifier] =
    for {
      setOfTasksComponentNode <- setOfTasksComponent.node
    } yield VDomModifier(
    div(
      cls := "d-flex",
      headerComponent.toggleStream.map{
        case true => div(
          navComponent,
        )
        case _ => div()
      },
      div(
        cls := "page-content",
        idAttr := "content",
        headerComponent.node,
        pageStreamF.map{
          case ReductionPage => reductionComponent
          case GeneratorPage => generatorComponent
//          case ContextTermPage => contextTermComponent.node.unsafeRunSync()
          case TasksPage    => tasksComponent
          case SetOfTasksPage => setOfTasksComponentNode
//          case ExamplePage2 => generatorComponent
//          case ExamplePage3 => generatorComponent
//          case EmptyPage => generatorComponent
        }
      ),
    ),
  )

}

object TopComponent {

  val pgeHndl = Handler.unsafe[Page](TasksPage)
  def init: IO[TopComponent] =(
    for {
      header                    <- HeaderComponent.init
//      contextTermComponent      <- ContextTermComponent.init
      setOfTasksComponent       <- SetOfTasksComponent.init
      tasksComponent            <- new TasksComponent(pgeHndl).node
      navComponent              <- new NavComponent(pgeHndl).node
      reductionComponent        <- new ReductionComponent().node
      generatorComponent        <- new GeneratingReductionTaskComponent().node
    } yield
      TopComponent(
        header,
//        contextTermComponent,
        setOfTasksComponent,
        navComponent,
        tasksComponent,
        reductionComponent,
        generatorComponent
      )).toIO
}
