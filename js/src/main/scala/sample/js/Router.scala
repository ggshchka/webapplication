package sample.js

import cats.effect.IO
import colibri.Observable
import outwatch.{VDomModifier, VNode}
import outwatch.dsl.{div, li}
import outwatch.reactive.handler.Handler

object ReductionRouter {
  sealed trait Page
  case object TasksPage extends Page
  case object ContextTermPage extends Page
  case object ReductionPage extends Page
  case object GeneratorPage extends Page

  case object ExamplePage1 extends Page
  case object ExamplePage2 extends Page
  case object SetOfTasksPage extends Page
  case object NextPage extends Page
  case object CurrentPage extends Page

  case object EmptyPage extends Page

  var pageStreamF: Handler[Page] = Handler.unsafe[Page](TasksPage)

  var isResetReductionPage: Handler[Boolean] = Handler.unsafe[Boolean](true)
  var isResetGeneratorPage: Handler[Boolean] = Handler.unsafe[Boolean](true)
}
