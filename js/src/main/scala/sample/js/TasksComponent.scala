package sample.js
import cats.effect.SyncIO
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import sample.js.ReductionRouter._

final private class TasksComponent  (pageHandler: Handler[Page]){
  var pageStream: Handler[Page] = pageHandler

  def node = SyncIO(
    div(
      cls := "d-flex flex-column col-9 min-vh-600",
      header(
        cls := "row d-flex justify-content-center subsystem-title",
        marginBottom := "1.14rem",
        h1("Система изучения редукции лямбда-термов"),
      ),
      div(
        cls := "container",
        div(
          cls := "row",
          a(
            cls := "col-md-12",
            href := "#",
            div(
              cls := "card mb-4 box-shadow task-title",
              "Редукция термов",
            ),
            onClick(ReductionPage) --> pageStreamF
          ),
          a(
            cls := "col-md-12",
            href := "#",
            div(
              cls := "card mb-4 box-shadow task-title",
              "Генерация задания на редукцию",
            ),
            onClick(GeneratorPage) --> pageStreamF
          ),
          a(
            cls := "col-md-12",
            href := "#",
            div(
              cls := "card mb-4 box-shadow task-title",
              "Заполнение пропусков в терме",
            ),
            onClick(ContextTermPage) --> pageStreamF
          ),
          a(
            cls := "col-md-12",
            href := "#",
            div(
              cls := "card mb-4 box-shadow task-title",
              "Набор заданий",
            ),
            onClick(SetOfTasksPage) --> pageStreamF
          ),
        ),
      ),
    )
  )
}