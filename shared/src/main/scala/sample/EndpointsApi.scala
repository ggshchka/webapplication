package sample

import endpoints4s.algebra._
import io.circe.{Decoder, Encoder}
import sample.model._

trait EndpointsApi  extends Endpoints with circe.JsonEntitiesFromCodecs {

  val generTermEndpointTest: Endpoint[Unit, String] = endpoint(
    get(path / "task2"),
    ok(textResponse)
  )

  val reductionEndpoint: Endpoint[TermForRqRs, Option[List[TermForRqRs]]] = endpoint(
    post(path / "reduction", jsonRequest[TermForRqRs]),
    ok(jsonResponse[Option[List[TermForRqRs]]])
  )

  val generateTermEndpoint: Endpoint[CountOfSteps, Option[TermForRqRs]] = endpoint(
    post(path / "generator" / "generateTerm", jsonRequest[CountOfSteps]),
    ok(jsonResponse[TermForRqRs]).orNotFound()
  )

  val evaluatedTermEndpoint: Endpoint[TermsForChecking, Option[ResultOfEvaluating]] = endpoint(
    post(path / "generator" / "getResult", jsonRequest[TermsForChecking]),
    ok(jsonResponse[ResultOfEvaluating]).orNotFound()
  )

  val generateTasksEndpoint: Endpoint[Unit, List[TasksForRqRs]] = endpoint(
    get(path / "generateTasks"),
    ok(jsonResponse[List[TasksForRqRs]])
  )

  val performTaskEndpoint: Endpoint[TasksForRqRs, TaskResult] = endpoint(
    post(path / "performTasks", jsonRequest[TasksForRqRs]),
    ok(jsonResponse[TaskResult])
  )

}
