package sample.model

import io.circe.{Decoder, Encoder}
import io.circe.generic.auto._
import cats.syntax.functor._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax.EncoderOps

trait TasksForRqRs

case class ContextTask(
                        contextTermList: List[String],
                        evaluatedTerm: String,
                        redStrat: String,
                        clientAnswer: List[String]
                      )  extends TasksForRqRs

case class ReductionTask(
                        term: String,
                        redStrat: String,
                        clientAnswer: String
                      )  extends TasksForRqRs

object TasksForRqRs {
  implicit val encodeTasks: Encoder[TasksForRqRs] = Encoder.instance {
    case t @ ReductionTask(_,_,_) => t.asJson
    case c @ ContextTask(_,_,_,_) => c.asJson
  }

  implicit val decodeTasks: Decoder[TasksForRqRs] =
    List[Decoder[TasksForRqRs]](
      Decoder[ReductionTask].widen,
      Decoder[ContextTask].widen,
    ).reduceLeft(_ or _)

}

case class TaskResult(message: String)
object TaskResult {
  implicit val enc: Encoder[TaskResult]  = deriveEncoder[TaskResult]
  implicit val dec: Decoder[TaskResult] =  deriveDecoder[TaskResult]
}

