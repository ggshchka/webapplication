package sample.model

import cats.syntax.functor._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax.EncoderOps
import io.circe.{Decoder, Encoder}

case class TermForRqRs(term: String, redStrat: String)
object TermForRqRs {
  implicit val enc: Encoder[TermForRqRs]  = deriveEncoder[TermForRqRs]
  implicit val dec: Decoder[TermForRqRs] =  deriveDecoder[TermForRqRs]
}

case class TermsForChecking(evaluatedByUser: TermForRqRs, generatedTerm: TermForRqRs)
object TermsForChecking {
  implicit val enc: Encoder[TermsForChecking]  = deriveEncoder[TermsForChecking]
  implicit val dec: Decoder[TermsForChecking] =  deriveDecoder[TermsForChecking]
}

case class ResultOfEvaluating(message: String, terms: List[TermForRqRs])
object ResultOfEvaluating {
  implicit val enc: Encoder[ResultOfEvaluating]  = deriveEncoder[ResultOfEvaluating]
  implicit val dec: Decoder[ResultOfEvaluating] =  deriveDecoder[ResultOfEvaluating]
}

case class CountOfSteps(cos: Int)
object CountOfSteps {
  implicit val enc: Encoder[CountOfSteps]  = deriveEncoder[CountOfSteps]
  implicit val dec: Decoder[CountOfSteps] =  deriveDecoder[CountOfSteps]
}
