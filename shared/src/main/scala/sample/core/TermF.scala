//package sample.core
//
//import cats.Functor
//import cats.syntax.functor._
//
//sealed trait TermF[A]
//case class VarF[A](name: String) extends TermF[A]
//case class AbstractionF[A](variable: A, body: A) extends TermF[A]
//case class ApplicationF[A](t1: A, t2: A) extends TermF[A]
//
//case class Fix[F[_]](unfix: F[Fix[F]])
//
//object TermF {
//  type TerF = Fix[TermF]
//  type Algebra[F[_], A] = F[A] => A
//
//  def cata[F[_]: Functor, A](algebra: F[A] => A)(fix: Fix[F]): A =
//    algebra(fix.unfix.map(cata(algebra)(_)))
//
//  implicit val termfFFunctor: Functor[TermF] = new Functor[TermF] {
//    def map[A, B](fa: TermF[A])(f: A => B): TermF[B] = fa match {
//      case VarF(x) => VarF[B](x)
//      case AbstractionF(v, b) => AbstractionF(f(v), f(b))
//      case ApplicationF(t1, t2) => ApplicationF(f(t1), f(t2))
//    }
//  }
//
//  val algebraString: Algebra[TermF, String] = {
//    case VarF(x) => x
//    case AbstractionF(v, b) => s"(λ.$v.$b)"
//    case ApplicationF(t1, t2) => s"$t1 $t2"
//  }
//
//  type VaF = VarF[Nothing]
////  def algebraFreeVars: Algebra[TermF, Set[VaF]] = {
////    case VarF(x) => Set(x)
////    case AbstractionF(v, body) => getFreeVars(body) - v
////    case Application(t1:Term[A], t2:Term[A]) => {
////      val t1Vars = getFreeVars(t1)
////      t1Vars ++ (getFreeVars(t2) -- t1Vars)
////    }
////    case _ => Set()
////  }
//
//
//  def evalString(query: TerF): String = cata(algebraString)(query)
//
//  def main(args: Array[String]): Unit = {
//    val t: Fix[TermF] =
//      Fix(ApplicationF(
//        Fix(AbstractionF(
//          Fix(VarF("x")),
//          Fix(VarF("x"))
//        )),
//        Fix(VarF("t"))
//      ))
//    println(t.unfix.)
//  }
//}